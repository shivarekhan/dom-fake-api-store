const form = document.querySelector("#form");

const firstName = document.getElementById("first-name");
const lastName = document.querySelector("#last-name");
const email = document.querySelector("#email");
const password = document.querySelector("#password");
const repeatPassword = document.querySelector("#re-password");
const tos = document.querySelector("#tos");
const validStatus = document.querySelector("#valid-message");

const invalidFirstName = document.querySelector("#invalid-first-name");
const invalidLastName = document.querySelector("#invalid-last-name");
const invalidEmail = document.querySelector("#invalid-email");
const invalidPassword = document.querySelector("#invalid-password");
const invalidRepeatPassword = document.querySelector("#invalid-re-password");
const invalidTos = document.querySelector("#invalid-tos");

function containsNumbers(name) {

    const specialCharacter = ['~', '!', '@', '#', '$', '%', '*', '(', ')', '+', '=', ':'];

    const haveNumbers = name.split("").reduce(function (haveNumberFlag, currentAlphabet) {

        if ((currentAlphabet >= 0 || currentAlphabet <= 9) || specialCharacter.indexOf(currentAlphabet) > -1) {
            haveNumberFlag = true;
        }
        return haveNumberFlag;
    }, false);

    return haveNumbers;
}


form.addEventListener('submit', function (event) {
    event.preventDefault();

    let validSignUpFlag = true;

    if (firstName.value.length == 0 || containsNumbers(firstName.value) == true) {
        invalidFirstName.textContent = `Invalid First Name Please enter without number and spaces`;
        invalidFirstName.style["color"] = "red";
        firstName.style["border-bottom"] = " 0.07rem solid red";
        validSignUpFlag = false;
    } else {
        firstName.style["border-bottom"] = " 0.07rem solid green";
        invalidFirstName.textContent = "";
    }

    if (lastName.value.length == 0 || containsNumbers(lastName.value) == true) {
        invalidLastName.textContent = "Invalid Last Name Please enter without number and spaces";
        invalidLastName.style["color"] = "red";
        lastName.style["border-bottom"] = " 0.07rem solid red";
        validSignUpFlag = false;
    } else {
        lastName.style["border-bottom"] = " 0.07rem solid green"
        invalidLastName.textContent = "";
    }

    const pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (pattern.test(email.value) === false) {
        invalidEmail.textContent = "Please enter a valid mail-in format of mail@domain.com"
        email.style["border-bottom"] = " 0.07rem solid red";
        validSignUpFlag = false;
    } else {
        invalidEmail.textContent = "";
        email.style["border-bottom"] = " 0.07rem solid green";
    }

    if (password.value.length <= 8) {
        invalidPassword.textContent = "Please ensure your password should be 9 characters long.";
        password.style["border-bottom"] = " 0.07rem solid red";
        validSignUpFlag = false;
    } else {
        invalidPassword.textContent = "";
        password.style["border-bottom"] = "0.07rem solid green";
    }

    if (repeatPassword.value != password.value || repeatPassword.value.length <= 8) {
        invalidRepeatPassword.textContent = "Password not matched or ensure that password should be 9 characters long";
        repeatPassword.style["border-bottom"] = " 0.07rem solid red";
        validSignUpFlag = false;
    } else {
        invalidRepeatPassword.textContent = "";
        repeatPassword.style["border-bottom"] = "0.07rem solid green";
    }

    if (tos.checked === false) {
        invalidTos.textContent = "Please accept the terms and conditions";
        validSignUpFlag = false;
    } else {
        invalidTos.textContent = "";

    }

    if (validSignUpFlag === true) {
        form.style["display"] = "none";
        validStatus.textContent = "Sign Up Success";
    }


}, false);
