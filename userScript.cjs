const API_KEY = "https://fakestoreapi.com/users";

const loader = document.querySelector("#loader");
const usersContainer = document.getElementById('users-container');


function stopLoader() {
    loader.remove();
}

function apiNotLoaded() {
    const error404 = document.createElement('div');
    error404.classList.add('error-404');

    const errorLogo = document.createElement('i');
    errorLogo.classList.add("fa-solid", "fa-exclamation", "fa-7x");

    const errorMessage = document.createElement("h3");
    errorMessage.textContent = "ERROR 404 RESPONSE NOT RECIVED";

    error404.appendChild(errorLogo);
    error404.appendChild(errorMessage);

    usersContainer.appendChild(error404);


}

function noUser() {

    const noUserFound = document.createElement('div');
    noUserFound.classList.add("no-user");

    const errorLogo = document.createElement('i');
    errorLogo.classList.add("fa-solid", "fa-circle-xmark", "fa-6x");

    const errorMessage = document.createElement("h3");
    errorMessage.textContent = "NO USER DATA RECIVED";

    noUserFound.appendChild(errorLogo);
    noUserFound.appendChild(errorMessage);

    usersContainer.appendChild(noUserFound);


}

function showUser(user) {
    const userElement = document.createElement('div');
    userElement.classList.add('user');

    const userIcon = document.createElement('i');
    userIcon.classList.add('fa-solid', 'fa-user', 'icon', 'fa-4x');

    const nameElement = document.createElement('h5');
    nameElement.classList.add("user-name");
    nameElement.textContent = `NAME -   ${user.name.firstname}  ${user.name.lastname}`;

    const emailElement = document.createElement('h5');
    emailElement.classList.add("user-email");
    emailElement.textContent = `EMAIL -   ${user.email}`;

    const mobileNumberElement = document.createElement('h5');
    mobileNumberElement.classList.add("user-mobile");
    mobileNumberElement.textContent = `PHONE -  ${user.phone}`;

    const addressElement = document.createElement("h5");
    addressElement.classList.add("user-address");
    addressElement.textContent = `Address - ${user.address.number}, ${user.address.street}, ${user.address.city}- ${user.address.zipcode}`

    userElement.appendChild(userIcon);
    userElement.appendChild(nameElement);
    userElement.appendChild(emailElement);
    userElement.appendChild(mobileNumberElement);
    userElement.appendChild(addressElement)

    usersContainer.appendChild(userElement);
}

function fetchUser(API_KEY) {
    return new Promise(function (resolve, reject) {

        fetch(API_KEY)
            .then(function (response) {
                if (!response.ok) {
                    reject();
                } else {
                    stopLoader();
                    return response.json();
                }
            })
            .then(function (userData) {
                if (userData == null) {
                    stopLoader();
                    noUser();
                } else {
                    resolve(userData);
                }
            })
            .catch(function (error) {
                stopLoader();
                apiNotLoaded();
                reject(error);
            })
    });
};


function renderUser(API_KEY) {
    fetchUser(API_KEY)
        .then(function (userData) {
            if (!Array.isArray(userData)) {
                showUser(userData);
            } else {
                userData.forEach(function (user) {
                    showUser(user);
                })
            }
        })
        .catch(function(error){
            noUser();
        })
}

renderUser(API_KEY);