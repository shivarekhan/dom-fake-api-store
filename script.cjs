const API_KEY = "https://fakestoreapi.com/products";

const loader = document.querySelector("#loader");
let flex = document.querySelector("#flex-3");

function stopLoader() {
    loader.remove();
}

function apiNotLoaded() {
    const error404 = document.createElement('div');
    error404.classList.add('error-404');

    const errorLogo = document.createElement('i');
    errorLogo.classList.add("fa-solid", "fa-exclamation", "fa-7x");

    const errorMessage = document.createElement("h3");
    errorMessage.textContent = "ERROR 404 RESPONSE NOT RECIVED";

    error404.appendChild(errorLogo);
    error404.appendChild(errorMessage);

    flex.appendChild(error404);


}

function noProduct() {

    const noUserFound = document.createElement('div');
    noUserFound.classList.add("no-product");

    const errorLogo = document.createElement('i');
    errorLogo.classList.add("fa-solid", "fa-circle-xmark","fa-6x");

    const errorMessage = document.createElement("h3");
    errorMessage.textContent = "NO PRODUCT DATA RECIVED";

    noUserFound.appendChild(errorLogo);
    noUserFound.appendChild(errorMessage);

    flex.appendChild(noUserFound);

}


function createProduct(currentProduct) {
    const card = document.createElement('div');
    card.classList.add('card');

    const upperCard = document.createElement('div');
    upperCard.classList.add('upper-card');
    card.appendChild(upperCard);

    const img = document.createElement('img');
    img.classList.add('img-size');
    img.src = currentProduct.image;
    img.alt = '';
    upperCard.appendChild(img);

    const lowerCard = document.createElement('div');
    lowerCard.classList.add('lower-card');
    card.appendChild(lowerCard);

    const productName = document.createElement('div');
    productName.classList.add('product-name');
    productName.textContent = currentProduct.title;
    lowerCard.appendChild(productName);

    const price = document.createElement('span');
    price.classList.add('price');
    const priceText = document.createElement('div');
    priceText.textContent = `price: $${currentProduct.price}`;
    price.appendChild(priceText);
    lowerCard.appendChild(price);

    const category = document.createElement('div');
    category.classList.add('category');
    category.textContent = `category: ${currentProduct.category}`;
    lowerCard.appendChild(category);

    const bottom = document.createElement('div');
    bottom.classList.add('bottom');
    lowerCard.appendChild(bottom);

    const buyButton = document.createElement('button');
    buyButton.classList.add('bottom-btn-1');
    buyButton.textContent = 'Buy';
    bottom.appendChild(buyButton);

    const addToCartButton = document.createElement('button');
    addToCartButton.classList.add('bottom-btn-2');
    addToCartButton.textContent = 'Add to Cart';
    bottom.appendChild(addToCartButton);

    flex.appendChild(card);
}

function getProducts(API_KEY) {

    return new Promise(function (resolve, reject) {

        fetch(API_KEY)
            .then(function (response) {
                if (!response.ok) {
                    reject();
                } else {
                    stopLoader();
                    return response.json();
                }
            })

            .then(function (productsData) {
                if (productsData == null) {
                    stopLoader();
                    noProduct();
                } else {
                    resolve(productsData);
                }
            })

            .catch(function (error) {
                stopLoader();
                apiNotLoaded();
                reject(error);
            })
    });
}

function renderProducts(API_KEY) {

    getProducts(API_KEY)
        .then(function (productsData) {

            if (!Array.isArray(productsData)) {

                console.log(productsData);
                createProduct(productsData);

            } else {
                productsData.map(function (currentProduct) {

                    createProduct(currentProduct);
                })
            }

        })

        .catch(function (error) {
            console.error(error);
        })
}

renderProducts(API_KEY);
